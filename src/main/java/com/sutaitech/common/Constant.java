package com.sutaitech.common;

public class Constant {
	//public static final String COOKIE_DOMAIN = null; //域
    public static final String USER_STATUS_KEY = "user_status";//用户名状态
    public static final String COOKIE_LOGIN_NAME_KEY = "login_un";//登录用户名
    public static final String COOKIE_LOGIN_TYPE_KEY = "login_type";//登录用户类型
    public static final String COOKIE_LOGIN_TOKEN = "login_token";//登录用户session-Key
    public static final Integer COOKIE_MAX_AGE = 94608000;//登录者非活动时的生命周期
    //初始化数据时默认账户
    public static final String INITDATA_DEFAULTUSER = "INIT_METROPOLO";
    //允许上传的图片大小，200kb
    public final static int UPLOAD_LOGO_MAX_SIZE = 204800;
    public final static String TEMP_UPLOAD_DIR = "/WEB-INF/upload";
}
